package br.com.keep.comparacred.loan

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Loan data class. Basically the Loan Model.
 *
 * @property id the ID of the Loan register. [Long] value. This is also the ID of the entity.
 * This ID is auto-generated.
 * @property date the date when the register was inserted in the database. [LocalDate] value.
 * @property institution the institution name. [String] value.
 * @property interestPerMonth the value of interest per month. [Float] value.
 * @property interestPerYear the value of interest per year. [Float] value.
 */
@Entity
data class Loan(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0L,
        val date: LocalDate = LocalDate.now(),
        val position: Int = 0,
        val institution: String = "",
        val interestPerMonth: Float = 0F,
        val interestPerYear: Float = 0F
    )