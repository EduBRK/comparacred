package br.com.keep.comparacred.loan

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Loan controller. Basically a CRUD for now. Maybe we don't need it...
 */
@RestController
@RequestMapping("loan")
class LoanController {

    @Autowired
    lateinit var loanService: LoanService

    /**
     * Finds a [Loan] by id.
     */
    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long) = loanService.findById(id)

    /**
     * Finds all [Loan] in the database.
     */
    @GetMapping
    fun findAll() = loanService.findAll()

    /**
     * Save's a new [Loan] entity. If the entity contains an id atribute, the function will throw an error. Use
     * the put mapping for updates.
     */
    @PostMapping
    fun save(@RequestBody loan: Loan) = loanService.save(loan)

    /**
     * Update an existing [Loan] entity. If the entity doesn't contains an id atribute, the function
     * will throw an error. Use the post mapping for new entities.
     */
    @PutMapping
    fun update(@RequestBody loan: Loan) = loanService.update(loan)

    /**
     * Delete's an existing [Loan] by it's id.
     */
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = loanService.delete(id)

}