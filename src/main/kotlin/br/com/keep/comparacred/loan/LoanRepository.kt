package br.com.keep.comparacred.loan

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
 * Crud Repository for the [Loan] data class.
 */
@Repository
interface LoanRepository: CrudRepository<Loan, Long>