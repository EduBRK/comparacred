package br.com.keep.comparacred.loan

import br.com.keep.comparacred.exceptions.ExceptionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Loan service. Contains all the [Loan] logic operations.
 */
@Service
class LoanService {

    @Autowired
    lateinit var loanRepository: LoanRepository

    /**
     * Finds a [Loan] by id.
     */
    fun findById(id: Long) = loanRepository.findById(id)

    /**
     * Finds all [Loan] in the database.
     */
    fun findAll(): Iterable<Loan> = loanRepository.findAll()

    /**
     * Save's a new [Loan] entity. If the entity contains an id atribute, the function will throw an error. Use
     * the put mapping for updates.
     */
    fun save(loan: Loan) =
            if (loan.id == 0L) loanRepository.save(loan) else throw ExceptionFactory.saveMissingId()

    /**
     * Update an existing [Loan] entity. If the entity doesn't contains an id atribute, the function
     * will throw an error. Use the post mapping for new entities.
     */
    fun update(loan: Loan) =
            if (loan.id != 0L) loanRepository.save(loan) else throw ExceptionFactory.updateWithoutId()

    /**
     * Delete's an existing [Loan] by it's id.
     */
    fun delete(id: Long) = loanRepository.deleteById(id)

}