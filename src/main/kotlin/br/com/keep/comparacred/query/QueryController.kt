package br.com.keep.comparacred.query

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Query Controller. Simple Crud controller, with some custom responses.
 */
@RestController
@RequestMapping("query")
class QueryController {

    @Autowired
    lateinit var queryService: QueryService

    /**
     * Finds a [Query] by id.
     */
    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long) = queryService.findById(id)

    /**
     * Finds all [Query] in the database.
     */
    @GetMapping
    fun findAll(): Iterable<Query> = queryService.findAll()

    /**
     * Save's a [Query] in the database. The return of this end-point will be the calculated values for the loan,
     * in a near future. If the Entity received has an ID, the end-point will return an error. Use the Put end-point
     * for updates.
     */
    @PostMapping
    fun save(@RequestBody query: Query) = queryService.save(query)

    /**
     * Updates a [Query] in the database. If the Entity received do not have an ID, the end-point will return an error.
     * Use the Post end-point for saves.
     */
    @PutMapping
    fun update(@RequestBody query: Query) = queryService.update(query)

    /**
     * Deletes a [Query] by it's id.
     */
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = queryService.delete(id)

}