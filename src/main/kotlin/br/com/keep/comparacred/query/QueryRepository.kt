package br.com.keep.comparacred.query

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface QueryRepository: CrudRepository<Query, Long>