package br.com.keep.comparacred.query

import br.com.keep.comparacred.bc.type.BancoCentralApiModalityType
import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Query data class.
 * This is a class used both for storing the query's received by the application.
 *
 * @property id the ID of the Query register. [Long] value. This is also the ID of the entity.
 * This ID is auto-generated.
 * @property bancoCentralApiModalityType the type of modality of credit being queried. Expects values from the
 * [BancoCentralApiModalityType] enum.
 * @property amount the amount asked on the loan. [BigDecimal] value.
 * @property parcels the amount of parcels used to pay the loan. [Int] value.
 */
@Entity
data class Query(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0L,
        val bancoCentralApiModalityType: BancoCentralApiModalityType?,
        val amount: BigDecimal?,
        val parcels: Int?
)