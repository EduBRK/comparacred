package br.com.keep.comparacred.query

import br.com.keep.comparacred.exceptions.ExceptionFactory
import br.com.keep.comparacred.loan.Loan
import br.com.keep.comparacred.loan.LoanService
import br.com.keep.comparacred.math.ComparaCredMath
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Query service. Contains all the [Query] logic operations.
 */
@Service
class QueryService {

    @Autowired
    lateinit var queryRepository: QueryRepository

    @Autowired
    lateinit var loanService: LoanService

    /**
     * Finds a [Query] by id.
     */
    fun findById(id: Long) = queryRepository.findById(id)

    /**
     * Finds all [Query] in the database.
     */
    fun findAll(): Iterable<Query> = queryRepository.findAll();

    /**
     * Save's a new [Query] entity. If the entity contains an id atribute, the function will throw an error. Use
     * the put mapping for updates.
     *
     * @return a [Iterable] of [Loan] with the calculated loan for this query
     */
    fun save(query: Query): Iterable<Loan> {
        if (query.id != 0L) throw ExceptionFactory.saveMissingId()
        queryRepository.save(query)
        return loanService.findAll()
    }

    /**
     * Update an existing [Query] entity. If the entity doesn't contains an id atribute, the function
     * will throw an error. Use the post mapping for new entities.
     */
    fun update(Query: Query) =
            if (Query.id != 0L) queryRepository.save(Query) else throw ExceptionFactory.updateWithoutId()

    /**
     * Delete's an existing [Query] by it's id.
     */
    fun delete(id: Long) = queryRepository.deleteById(id)

}