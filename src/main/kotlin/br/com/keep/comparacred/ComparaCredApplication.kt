package br.com.keep.comparacred

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

/**
 * Main application class
 */
@SpringBootApplication
@EnableFeignClients
class ComparaCredApplication

/**
 * Main starting function
 */
fun main(args: Array<String>) {
    runApplication<ComparaCredApplication>(*args)
}
