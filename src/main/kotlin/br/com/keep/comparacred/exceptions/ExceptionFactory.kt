package br.com.keep.comparacred.exceptions

/**
 * Static class with common [Exception] used in multiple classes.
 */
class ExceptionFactory {
    companion object {
        fun saveMissingId() = Exception("Cannot insert with an ID")
        fun updateWithoutId() = Exception("Cannot insert with an ID")
    }
}