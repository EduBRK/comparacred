package br.com.keep.comparacred.math

import java.math.BigDecimal
import java.util.*
import kotlin.math.pow

/**
 * Class that will group all mathematics operations of the ComparaCred application.
 */
class ComparaCredMath {

    /**
     * Calculates the compound interest rate for the asked value and parcels.
     */
    fun compoundInterest(amount: BigDecimal, rate: Double, parcels: Int) =
            amount.multiply(BigDecimal.valueOf(rate / (1 - (1 + rate).pow(-parcels))))

}