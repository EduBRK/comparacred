package br.com.keep.comparacred.credit

/**
 * Enumeration with the credit types accepted by the backend
 */
enum class CreditType {
    CONSIGNADO,
    IMOBILIARIO,
    PESSOAL,
    VEICULO
}