package br.com.keep.comparacred.bc

import br.com.keep.comparacred.bc.type.BancoCentralApiModalityType
import br.com.keep.comparacred.bc.type.BancoCentralApiType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

/**
 * Banco Central Controller.
 *
 * Used to access the Banco Central API as an Endpoint. Currently for testing purposes only.
 */
@RestController
@RequestMapping("bc")
class BancoCentralController {

    @Autowired
    lateinit var bancoCentralService: BancoCentralService

    /**
     * Extension for recovering a random value from a [IntRange]
     */
    fun IntRange.random() = Random().nextInt((endInclusive + 1) - start) +  start

    /**
     * Return a randomly chosen Daily Interest Rate from the [BancoCentralService]
     */
    @GetMapping("/daily")
    fun getDailyInterest(): BancoCentralClientResponse {

        val dailyModalities = BancoCentralApiModalityType.values()
                .filter { modalityType -> modalityType.bancoCentralApiType == BancoCentralApiType.DAILY }

        val randomModality = dailyModalities[(0 until dailyModalities.size).random()]

        return bancoCentralService.getInterestRate(randomModality)

    }

    /**
     * Return's the Monthly Service from the [BancoCentralService] (Currently only [BancoCentralApiModalityType.IMOBILIARIO])
     */
    @GetMapping("/monthly")
    fun getMonthlyInterest() = bancoCentralService.getInterestRate(BancoCentralApiModalityType.IMOBILIARIO)

}