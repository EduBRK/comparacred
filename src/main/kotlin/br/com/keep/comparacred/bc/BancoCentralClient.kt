package br.com.keep.comparacred.bc

import br.com.keep.comparacred.bc.type.BancoCentralApiFormatType
import br.com.keep.comparacred.bc.type.BancoCentralApiOrderType
import br.com.keep.comparacred.bc.type.BancoCentralApiSelectType
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

/**
 * Feign Client to access the Banco Central API.
 */
@FeignClient(name = "bc", url = "https://olinda.bcb.gov.br/olinda/servico/taxaJuros/versao/v2/odata")
@Component
interface BancoCentralClient {

    /**
     * Returns the daily interest.
     *
     * @property format The returning format of the API. Default's to json. For more values, use
     * [BancoCentralApiFormatType]
     * @property top [Int] value of amount of records that will return from the BancoCentral API. Defaults to 10.
     * @property orderBy [String] value that defines the order the API should return it's values.
     * Defaults to "Posicao". For more values, use [BancoCentralApiOrderType].
     */
    @RequestMapping("/TaxasJurosDiariaPorInicioPeriodo")
    fun getDailyInterest(
            @RequestParam(name = "\$format") format: String? = BancoCentralApiFormatType.JSON.value,
            @RequestParam(name = "\$top") top: Int? = 1,
            @RequestParam(name = "\$select") select: String? = BancoCentralApiSelectType.DAILY.value,
            @RequestParam(name = "\$orderby") orderBy: String? = BancoCentralApiOrderType.POSICAO.value
    ): BancoCentralClientResponse

    /**
     * Returns the monthly interest.
     *
     * @property format The returning format of the API. Default's to json. For more values, use
     * [BancoCentralApiFormatType]
     * @property top [Int] value of amount of records that will return from the BancoCentral API. Defaults to 10.
     * @property orderBy [String] value that defines the order the API should return it's values.
     * Defaults to "Posicao". For more values, use [BancoCentralApiOrderType].
     */
    @RequestMapping("/TaxaJurosMensalPorMes")
    fun getMonthlyInterest(
            @RequestParam(name = "\$format") format: String? = BancoCentralApiFormatType.JSON.value,
            @RequestParam(name = "\$top") top: Int? = 10,
            @RequestParam(name = "\$select") select: String? = BancoCentralApiSelectType.MONTHLY.value,
            @RequestParam(name = "\$orderby") orderBy: String? = BancoCentralApiOrderType.POSICAO.value
    ): BancoCentralClientResponse

}