package br.com.keep.comparacred.bc

import br.com.keep.comparacred.bc.type.BancoCentralApiModalityType
import br.com.keep.comparacred.bc.type.BancoCentralApiType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * BancoCentral service. Contains all the Banco Central logic operations.
 */
@Service
class BancoCentralService {

    private val bancoCentralClient: BancoCentralClient

    @Autowired
    constructor(bancoCentralClient: BancoCentralClient) {
        this.bancoCentralClient = bancoCentralClient
    }

    /**
     * Recovers the interest rate for the modality asked.
     *
     * @param bancoCentralApiModalityType The modality to be recovered. [BancoCentralApiModalityType] enum.
     */
    fun getInterestRate(bancoCentralApiModalityType: BancoCentralApiModalityType) =
            when (bancoCentralApiModalityType.bancoCentralApiType) {
                BancoCentralApiType.DAILY -> bancoCentralClient.getDailyInterest()
                BancoCentralApiType.MONTHLY -> bancoCentralClient.getMonthlyInterest()
            }

}