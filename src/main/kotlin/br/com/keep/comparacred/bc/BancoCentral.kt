package br.com.keep.comparacred.bc

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Banco Central Response Data Class for client use.
 * This data class groups both responses from the [BancoCentralClient].
 *
 * @property inicioPeriodo The starting period from the register. [String] value.
 * @property fimPeriodo The ending period from the register. [String] value.
 * @property segmento The segment of the register. [String] value.
 * @property mes The month of the register. This property is only applied to the Monthly end-point. [String] value.
 * @property modalidade The modality of the register. [String] value.
 * @property posicao The position of the financial institution for the aforementioned period, in ascending order.
 * [Int] value.
 * @property instituicaoFinanceira The name of the financial institution. [String] value.
 * @property taxaJurosAoMes The interest per month for this register. [Double] value.
 * @property taxaJurosAoAno The interert per year for this register. [Double] value.
 * @property cnpj8 The cnpj of the financial institution. [String] value.
 * @property anoMes The year of the register. This property is only applied to the Monthly end-point. [String] value.
 */
data class BancoCentral(
        val inicioPeriodo: String? = DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()),
        val fimPeriodo: String? = DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()),
        val segmento: String? = "",
        val mes: String? = DateTimeFormatter.ofPattern("MM").format(LocalDate.now()),
        val modalidade: String? = "",
        val posicao: Int? = 1,
        val instituicaoFinanceira: String? = "",
        val taxaJurosAoMes: Double? = 0.0,
        val taxaJurosAoAno: Double? = 0.0,
        val cnpj8: String? = "",
        val anoMes: String? = ""
)