package br.com.keep.comparacred.bc.type

enum class BancoCentralApiSelectType(val value: String) {
    DAILY("InicioPeriodo,FimPeriodo,Segmento,Modalidade,Posicao,InstituicaoFinanceira,TaxaJurosAoMes," +
            "TaxaJurosAoAno,cnpj8"),
    MONTHLY("Mes,Modalidade,Posicao,InstituicaoFinanceira,TaxaJurosAoMes,TaxaJurosAoAno,cnpj8,anoMes")
}