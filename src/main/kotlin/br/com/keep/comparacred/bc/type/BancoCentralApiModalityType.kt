package br.com.keep.comparacred.bc.type

/**
 * Enumeration with the credit types accepted by the backend
 */
enum class BancoCentralApiModalityType(val bancoCentralApiType: BancoCentralApiType, val queryValue: String) {
    CONSIGNADO(BancoCentralApiType.DAILY, "CRÉDITO PESSOAL CONSIGNADO PÚBLICO - PRÉ-FIXADO"),
    IMOBILIARIO(BancoCentralApiType.MONTHLY, "FINANCIAMENTO IMOBILIÁRIO COM TAXAS DE MERCADO - PRÉ-FIXADO"),
    PESSOAL(BancoCentralApiType.DAILY, "CRÉDITO PESSOAL NÃO CONSIGNADO - PRÉ-FIXADO"),
    VEICULO(BancoCentralApiType.DAILY,"AQUISIÇÃO DE VEÍCULOS - PRÉ-FIXADO")
}