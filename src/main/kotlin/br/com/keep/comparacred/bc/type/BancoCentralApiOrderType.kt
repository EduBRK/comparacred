package br.com.keep.comparacred.bc.type

/**
 * Enumeration of the possible values used for ordering the return of the Banco Central API.
 * This enum should always represent the values of [BancoCentral], but the first letter must be capitalized.
 *
 * The values of this enum should accessed by the functions asc() and desc().
 */
enum class BancoCentralApiOrderType(val value: String) {

    INICIO_PERIODO("InicioPeriodo"),
    FIM_PERIODO("FimPeriodo"),
    SEGMENTO("Segmento"),
    MES("Mes"),
    MODALIDADE("Modalidade"),
    POSICAO("Posicao"),
    INSTITUICAO_FINANCEIRA("InstituicaoFinanceira"),
    TAXA_JUROS_AO_MES("TaxaJurosAoMes"),
    TAXA_JUROS_AO_ANO("TaxaJurosAoAno"),
    CNPJ8("Cnpj8"),
    ANO_MES("AnoMes");

}