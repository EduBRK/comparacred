package br.com.keep.comparacred.bc.type

/**
 * Enum to define where the Banco Central data will be recovered.
 */
enum class BancoCentralApiType {
    DAILY,
    MONTHLY
}