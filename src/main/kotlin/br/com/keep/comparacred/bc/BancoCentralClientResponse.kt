package br.com.keep.comparacred.bc

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class BancoCentralClientResponse (
        @JsonProperty("@odata.context") val dataContext: String = "",
        val value: Array<BancoCentral> = emptyArray()
){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BancoCentralClientResponse

        if (dataContext != other.dataContext) return false
        if (!Arrays.equals(value, other.value)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = dataContext.hashCode()
        result = 31 * result + Arrays.hashCode(value)
        return result
    }

}