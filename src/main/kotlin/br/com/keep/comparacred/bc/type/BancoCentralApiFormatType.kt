package br.com.keep.comparacred.bc.type

/**
 * Enumeration of the possible returns of the Banco Central API.
 */
enum class BancoCentralApiFormatType(val value: String) {
    JSON("json"),
    XML("xml"),
    CSV("text/csv"),
    TEXT("text/plain"),
    HTML("text/html")
}